/*
When threads share access to a common object, they can conflict
with each other.  To demonstrate the problems that can arise,
we will investigate a sample program in which multiple threads
manipulate a bank account.
We construct a bank account that starts with a zero balance.  We
create two sets of threads:
-- Each thread in the first set repeatedly deposits $100
-- Each thread in the second set repeatedly withdraws $100
*/
/** This program runs threads that deposit and withdraw
    money from the same bank account
  */
public class BankAccountThreadRunner {
	public static void main(String args[]) {
		BankAccount account = new BankAccount();
		final double AMOUNT = 100;
		final int REPETITIONS = 10;
		final int THREADS = 10;

		for (int i = 1; i <= THREADS; i++) {
			  DepositRunnable d = new DepositRunnable(
				   account, AMOUNT, REPETITIONS);
			  WithdrawRunnable w = new WithdrawRunnable(
				   account, AMOUNT, REPETITIONS);

			  Thread dt = new Thread(d);
			  Thread wt = new Thread(w);

			  dt.start();
			  wt.start();
	    }
    }
}

